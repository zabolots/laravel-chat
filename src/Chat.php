<?php

namespace Zabolots\LaravelChat;

class Chat
{
    public function render()
    {
        if (config('chat.show')) {
            switch (config('chat.provider')) {
                case 'jivosite':
                    return view('chat::jivosite');
                case 'siteheart':
                    return view('chat::siteheart');
                default:
                    return 'Unknown chat provider.';
            }
        }
    }
}