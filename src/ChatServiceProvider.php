<?php

namespace Zabolots\LaravelChat;

use Illuminate\Support\ServiceProvider;

class ChatServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/chat.php' => config_path('chat.php'),
        ]);

        $this->loadViewsFrom(__DIR__ . '/resources/views','chat');

        \App::singleton('chat', function () {
            return new Chat;
        });
    }

}