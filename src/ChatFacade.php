<?php

namespace Zabolots\LaravelChat;

use Illuminate\Support\Facades\Facade;

class ChatFacade extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return Chat::class;
    }
}