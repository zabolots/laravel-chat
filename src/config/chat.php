<?php

    return [
        'provider' => env('CHAT_PROVIDER', 'jivosite'),
        'show' => env('CHAT_SHOW', true),
        'id' => env('CHAT_ID', 0),
    ];
