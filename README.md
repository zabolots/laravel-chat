## About

Laravel Chat allows you to add an online chat with visitors of your site.
Chat based on JivoSite (default) or SiteHeart.

## Install

1. `composer require zabolots/laravel-chat:dev-master`

2. `php artisan vendor:publish --provider="Zabolots\LaravelChat\ChatServiceProvider"`
 
3. Next you must to register on JivoSite by this url: https://goo.gl/yA8aLW and add your site
in admin panel here: https://goo.gl/Ezd7Wt

4. If you have Laravel 5.5 or higher you can skip this step: open config/app.php and add `\Zabolots\LaravelChat\ChatServiceProvider::class,` to providers array and `'Chat' => \Zabolots\LaravelChat\ChatFacade::class,` to aliases array.

## Configure

1. ADD `CHAT_PROVIDER=jivosite` or `CHAT_PROVIDER=siteheart` to `.env` file.
2. Add `CHAT_ID=<your_id>` to `.env` file. 
For JivoSite you can find <your_id> in javascript code (look at line with widget_id).
For SiteHeart you can find <your_id> in SiteHeart (Setup -> Widjet -> Code and look at line with widget_id).
3. Add `{!! Chat::render() !!}` to your blade template file.

## How to switch off chat
You can just add `CHAT_SHOW=false` to `.env` file if you want to switch off chat on your site.